#' Descricao do cad
#'
#' @return
#' @export
#'
#' @examples
DesCad <- function(tipoCAD = as.character(NA)){
  
  # Elimina espacos
  tipoCAD %<>% 
    str_replace_all(
      pattern = " ", 
      replacement = ""
    )
  
  # Busca descricao correspondente
  descricao <- case_when(
    tipoCAD == "mst" ~ "mastologia",
    tipoCAD == "url" ~ "urologia", 
    tipoCAD == "ccv" ~ "cardiovascular",
    tipoCAD == "civ" ~ "cirurgia vascular",
    tipoCAD == "cip" ~ "plastica",
    tipoCAD == "oto" ~ "otorrino",
    tipoCAD == "cit" ~ "toracica",
    tipoCAD == "nci" ~ "neuro",
    tipoCAD == "sgo" ~ "gineco/obst",
    tipoCAD == "pro" ~ "proctologia",
    tipoCAD == "cig" ~ "cirurgia geral",
    tipoCAD == "cad" ~ "aparelho digestivo",
    tipoCAD == "cii" ~ "pediátrica",
    tipoCAD == "car" ~ "cardiologia",
    tipoCAD == "crc" ~ "craniomaxilo",
    tipoCAD == "der" ~ "dermatololgia",
    tipoCAD == "gas" ~ "gastro",
    tipoCAD == "nef" ~ "nefrologia",
    tipoCAD == "ort" ~ "ortopedia",
    tipoCAD == "mei" ~ "medicina interna",
    tipoCAD == "hem" ~ "hematologia",
    tipoCAD == "rad" ~ "radiologia",
    tipoCAD == "pne" ~ "pneumologia",
    tipoCAD == "oft" ~ "oftalmologista",
    tipoCAD == "onp" ~ "oncologia pediatrica",
    tipoCAD == "cib" ~ "bucomaxilofacial",
    TRUE             ~ as.character(NA)
  )
  
  # Retorno da funcao
  return(descricao) 
}
####
## Fim
#