#' Optimal scale para variaveis continuas e categoricas
#'
#' @param vetor 
#'
#' @return
#' @export
#'
#' @examples
ScaleOptimal <- function(dados){
  
  dados %<>% 
    mutate_at(
      .vars = vars(starts_with("CSV")),
      .funs = function(vetor){
        opscale(
          x.qual = vetor,
          x.quant = if(!is.character(vetor)){
            as.numeric(vetor)
          } else {
            1L:length(vetor)
          },
          level = ifelse(
            test = is.ordered(vetor),
            yes = 2L,
            no = 1L
          ),
          process = ifelse(
            test = (is.logical(vetor) | is.factor(vetor)),
            yes = 2L,
            no = 1L
          )
        )$os
      }
    )  
  
  # Retorno da funcao
  return(dados)  
}
####
## Fim
#