#' Ranking PLS DA
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
RankingPlsDA <- function(dados){
  
  # Cria modelo PlsDA
  modelo <- ChamarFuncao(
    dados = dados,
    funcao = "ClassificadorPlsDA"
  )  
  
  # bk = sum(Cia * Wia)
  Ca_Wka <- modelo$explained_variance$X * modelo$loadings$X
  
  # Calcula score de importancia
  rankingVariaveis <- apply(
    X = Ca_Wka,
    MARGIN = 1L,
    FUN = function(linha){
      linha %>% 
        sum() %>% 
        abs()
    }
  ) 
  
  # Monta ranking de importancia de variaveis
  rankingVariaveis <- tibble(
    Variavel = names(rankingVariaveis),
    Score = as.double(rankingVariaveis)
  ) %>% 
    arrange(desc(Score))
  
  # Retorno da funcao
  return(rankingVariaveis)
}
####
## Fim
#