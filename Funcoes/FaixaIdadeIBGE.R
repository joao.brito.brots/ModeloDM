#' Faixa Idade IBGE
#'
#' @param idade 
#'
#' @return
#' @export
#'
#' @examples
FaixaIdadeIBGE <- function(idade){
  
  # Apurar faixa de idade
  faixaIdadeIBGE = case_when(
    idade < 19  ~ "00-18",
    idade < 25  ~ "19-24",
    idade < 30  ~ "25-29",
    idade < 35  ~ "30-34",
    idade < 40  ~ "35-39",
    idade < 45  ~ "40-44",
    idade < 50  ~ "45-49",
    idade < 55  ~ "50-54",
    idade < 60  ~ "55-59",
    idade < 65  ~ "60-64",
    idade < 70  ~ "65-69",
    idade < 75  ~ "70-74",
    idade < 80  ~ "75-79",
    idade < 85  ~ "80-84",
    idade < 90  ~ "85-89",
    idade < 95  ~ "90-94",
    idade < 100 ~ "95-99",
    TRUE        ~ "MAIOR 99"
  ) %>% 
    factor(ordered = TRUE)
  
  # Retorno da funcao
  return(faixaIdadeIBGE)
}
####
## Fim
#