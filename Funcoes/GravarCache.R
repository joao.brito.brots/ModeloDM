#' Guarda dados em Cache
#'
#' @param dados 
#' @param arquivo 
#'
#' @return
#' @export
#'
#' @examples
GravarCache <- function(dados, arquivo){
  
  # Monta nome completo (endereco + arquivo)
  arquivo <- str_c("Cache/", arquivo)
  
  # Se for tibble
  if("tbl_df" %in% class(dados)){
    # Grava dados
    write_fst(
      x = dados,
      path = str_c(arquivo, ".fst"),
      compress = 100L
    )
  } else {
    # Grava dados
    write_rds(
      x = dados,
      file = str_c(arquivo, ".RDS"),
      compress = "gz"
    )
  }
  
  # Libera memoria
  gc(reset = TRUE, full = TRUE, verbose = FALSE)
  
  # Retorna dados
  return(dados)
}
####
## Fim
#