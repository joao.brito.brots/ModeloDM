#' Exportar layout dos dados
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
ExportarLayout <- function(dados){
  
  # Identifica tipos de campos
  tiposCampos <- dados[1,] %>% 
    sapply(
      FUN = function(campo){
        tipoCampo <- campo %>% 
          class() %>% 
          str_c(., collapse = "/") %>% 
          as.character() 
        
        tipoCampo <- case_when(
          (tipoCampo == "POSIXct/POSIXt") ~ "Data e hora",
          (tipoCampo == "Date")           ~ "Data",
          (tipoCampo == "Period")         ~ "Data",
          (tipoCampo == "numeric")        ~ "Real",
          (tipoCampo == "integer")        ~ "Inteiro",
          (tipoCampo == "ordered/factor") ~ "Categorico",
          (tipoCampo == "factor")         ~ "Categorico",
          (tipoCampo == "logical")        ~ "Logico",
          TRUE                            ~ "Texto"
        ) 
        
        return(tipoCampo)
      }
    )
  
  # Remove labels 
  names(tiposCampos) <- NULL
  
  # Identifica donimios dos campos
  dominioCampos <- dados %>% 
    sapply(
      FUN = function(campo){
        
        tipoCampo <- campo %>% 
          class() %>% 
          str_c(., collapse = "/") %>% 
          as.character() 

        if(str_detect(tipoCampo, "factor") | n_distinct(campo) < 4){
          dominioCampo <- campo %>% 
            as_tibble() %>% 
            CountPercAcum(value) %>% 
            mutate(
              Perc = Perc %>% 
                round(1),
              Dominio = str_c(value, " (", Perc, "%)")
            ) %>% 
            pull(Dominio) %>% 
            str_c(., collapse = "; ")
        } else {
          dominioCampo <- str_c(
            "De ",
            round(min(campo, na.rm = TRUE), 4),
            " ate ",
            round(max(campo, na.rm = TRUE), 4)
          )
        }
        
        return(dominioCampo)
      }
    ) %>% 
    str_squish()

  # Monta layout
  layout <- dados %>% 
    colnames() %>% 
    as_tibble() %>% 
    rename(
      Campos = value
    ) %>% 
    mutate(
      Tipo = tiposCampos,
      Dominio = dominioCampos
    ) %>% 
    left_join(
      dados %>% 
        summarise_all(
          function(coluna) {
            ((1L - length(na.omit(coluna)) / length(coluna)) * 100L)
          }
        ) %>% 
        t() %>% 
        as.data.frame() %>% 
        rownames_to_column(
          var = "Campos"
        ) %>% 
        as_tibble() %>% 
        rename(PercNA = V1),
      by = "Campos"
    ) 
  
  # Retorno da funcao
  return(layout)
}
####
## Fim 
# 