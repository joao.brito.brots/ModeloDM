#' Gera ranking de variaveis - Qui-Quadrado
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
RankingQuiQuadrado <- function(dados){
  
  # Apura ranking
  rankingVariaveis <- chi.squared(
    formula = Classe ~ .,
    data = dados
  ) %>% 
    rownames_to_column(
      var = "Variavel"
    ) %>% 
    as_tibble() %>% 
    rename(
      Score = attr_importance
    ) %>% 
    arrange(desc(Score))
  
  # Retorno da funcao
  return(rankingVariaveis)
}
####
## Fim
#
