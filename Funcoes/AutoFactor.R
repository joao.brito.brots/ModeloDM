#' Verifica se muda para factor
#'
#' @param vetorTexto 
#'
#' @return
#' @export
#'
#' @examples
AutoFactor <- function(vetorTexto){
  
  # Se possui valores distintos para categorizar
  if((length(unique(vetorTexto)) / length(vetorTexto)) < 0.5){
    vetorTexto <- factor(vetorTexto)
  }
  
  # Retorno da funcao
  return(vetorTexto)
}
####
## Fim
#