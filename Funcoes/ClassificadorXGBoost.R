#' Cria modelo Extreme Gradient Boosting
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorXGBoost <- function(dados){
  
  # Limpa memoria
  gc(reset = TRUE, full = TRUE)

  # Cria modelo preditivo
  modelo <- xgboost(
    data = dados %>% 
      select(-Classe) %>% 
      as.matrix(),
    label = (as.numeric(dados$Classe) - 1),
    params = list(
      max_depth = 2L, 
      eta = 1L, 
      nthread = parallel::detectCores()
    ),
    nrounds = 2L, 
    verbose = 0L, 
    objective = "binary:logistic"
  )

  # Limpa memoria
  gc(reset = TRUE, full = TRUE)
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#
