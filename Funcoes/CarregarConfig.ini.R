#' Carregar Config.ini
#'
#' @return
#' @export
#'
#' @examples
CarregarConfig.ini <- function() {
  
  # Se nao existe arquivo de configuracoes
  if(!file.exists("Config.ini")){
    return(list())
  }
  
  # Carrega parametros de configuracao do sistema
  parSys <- read.ini(
    filepath = "Config.ini",
    encoding = "UTF-8"
  )

  # Busca as listas
  for(i in 1:1000){
    
    # Se finalizou
    if(is.null(unlist("["(parSys, i)))){
      break()
    }

    # Busca as sublistas
    for(j in 1:1000){

      # Conteudo
      conteudo <- unlist("[["(parSys, i)[j])
    
      # Se finalizou
      if(is.null(conteudo)){
        break()
      }
      
      # Se for numerico
      if(suppressWarnings(!any(is.na(as.numeric(conteudo))))){
        "[["(parSys, i)[j] <- as.numeric(conteudo)
      } else if(suppressWarnings(!any(is.na(as.logical(conteudo))))){
        "[["(parSys, i)[j] <- as.logical(conteudo)
      }
      
    }
  }

  # Retorno da funcao
  return(parSys)
}
####
## Fim
#
