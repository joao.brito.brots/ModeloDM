#' Balanceamento de dados por classe DBSMOTE
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorDBSMOTE <- function(dados){
  
  # Faz balanceamento dos dados
  dadosBalanceados <- oversample(
    dataset = dados %>% 
      mutate_all(FactorLogicalToLogical) %>% 
      as.data.frame(),
    method = "DBSMOTE",
    classAttr = "Classe",
    ratio = parSys$Balanceador$Ratio
  ) %>% 
    as_tibble() %>% 
    mutate_all(LogicalToFactorLogical)
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
