#' Buscar dados de Cache
#'
#' @param dados 
#' @param arquivo 
#'
#' @return
#' @export
#'
#' @examples
BuscarCache <- function(arquivoCache){
  
  # Libera memoria
  gc(reset = TRUE, full = TRUE, verbose = FALSE)
  
  # Monta nome completo (endereco + arquivo)
  arquivoCache <- dir(
    path = "Cache/",
    pattern = str_c("^", arquivoCache, ".*"),
    full.names = TRUE
  )[1]
  
  # Se existe arquivo de cache
  if(is.na(arquivoCache)){
    return(tibble())
  }  
  
  # Se for tibble
  if(str_detect(arquivoCache, "fst")){
    
    # Busca dados
    dados <- read_fst(
      path = arquivoCache
    ) %>% 
      as_tibble()
    
  } else {
    
    # Busca dados
    dados <- read_rds(
      file = arquivoCache
    ) 
  }  
  
  # Libera memória
  gc(reset = TRUE, verbose = FALSE, full = TRUE)
  
  # Retorno da funcao
  return(dados)
}
####
## Fim
#