#' Balanceamento de dados por classe SMOTE
#'
#' @param dados 
#' @param classe 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorSMOTE <- function(dados){
  
  # Faz balanceamento dos dados
  dadosBalanceados <- SMOTE(
    form = Classe ~ .,
    data = dados %>% 
      as.data.frame(),
    perOver = parSys$Balanceador$SMOTE_perc.over,
    k = parSys$Balanceador$SMOTE_k
  ) %>% 
    as_tibble()

  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
