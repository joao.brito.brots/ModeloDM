#' Balanceamento de dados por classe Naive
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorNaive <- function(dados){
  
  # Busca classe majoritaria
  countClass <- dados %>% 
    count(Classe) %>% 
    arrange(n) %>% 
    slice(1)
  
  # Dados balanceados
  dadosBalanceados <- dados %>% 
    filter(
      (Classe == countClass$Classe)
    ) %>% 
    bind_rows(
      dados %>% 
        filter(
          (Classe != countClass$Classe)
        ) %>% 
        sample_n(countClass$n)
    )
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#
