#' Gera ranking de variaveis - Gain Ratio
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
RankingGainRatio <- function(dados){
  
  # Apura ranking
  rankingVariaveis <- gain.ratio(
    formula = Classe ~ .,
    data = dados
  ) %>% 
    rownames_to_column(
      var = "Variavel"
    ) %>% 
    as_tibble() %>% 
    rename(
      Score = attr_importance
    ) %>% 
    arrange(desc(Score))
  
  # Retorno da funcao
  return(rankingVariaveis)
}
####
## Fim
#
