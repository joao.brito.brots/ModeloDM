#' Cria grafico de correlacoes
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
GraficoCorrelacoes <- function(dados){
  
  # Apura correlacoes
  correlacoes <- dados %>% 
    EliminarVariaveisRedundantes() %>% 
    select_if(is.numeric) %>% 
    as.matrix() %>% 
    cor()
  
  # Cria grafico de correlacoes
  corrplot(
    corr = correlacoes,
    type = "upper",
    method = "color",
    tl.col = "black",
    tl.cex = 0.5, 
    diag = FALSE,
    tl.srt = 90,
    addCoef.col = "black",
    number.cex = 0.5,
    number.digits = 1
  )
  
  # Funcao sem retorno
  invisible()
}
#### 
## Fim
#