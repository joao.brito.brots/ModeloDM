#' Palavras para serem removidas em text mining
#'
#' @return
#' @export
#'
#' @examples
BuscarStopWords <- function(){
  
  # Stop words
  stopWords <- c(stopwords(kind = "pt"), letters)
  
  # Palavras para serem removidas em text mining
  removerPalavras <- stopWords %>% 
    iconv(
      from = stri_enc_detect(
        str = stopWords,
        filter_angle_brackets = FALSE
      ) %>% 
        bind_rows() %>% 
        slice(1) %>% 
        pull(Encoding),
      to = "UTF-8"
    ) %>% 
    TratarAlfanumericos()
  
  # Retorn da funcao
  return(removerPalavras)
}
####
## Fim
#