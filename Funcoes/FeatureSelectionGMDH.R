#' Selecao de Variaveis com GMDH
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
FeatureSelectionGMDH <- function(dados, kFolds = 10L, desempenho = FALSE){
  
  # Particiona em 10 k folds
  particoes <- createFolds(
    y = dados$Classe, 
    k = kFolds, 
    list = TRUE, 
    returnTrain = FALSE
  )
  
  # Selecao variaveis
  Msg(yellow("Selecao variaveis GMDH"))
  variaveis <- lapply(
    X = names(particoes),
    FUN = function(particao){
      
      variaveis <- try(
        expr =  ClassificadorGMDH(
          dados = dados %>% 
            slice(-particoes[[particao]])
        )$features
      )  
      
      if(Erro(variaveis)){
        variaveis <- character()
      } 
      
      tibble(
        Variaveis = variaveis
      )
    }
  ) %>% 
    bind_rows() %>% 
    count(Variaveis) %>% 
    transmute(
      Variaveis,
      Score = (n / kFolds * 100L)
    ) %>% 
    arrange(desc(Score))
  
  # Se calcula desempenho
  if(desempenho){
    
    Msg(yellow("Calculando desempenho da Selecao variaveis GMDH"))
    
    # Cria modelo
    modelo <- train(
      form = Classe ~ .,
      data = dados %>% 
        select(Classe, !!variaveis$Variaveis),
      method = PolynomialNeuralNetwork,
      metric = "ROC",
      maximize = TRUE,
      trControl = trainControl(
        method = "cv",
        number = 10L,
        savePredictions = "final",
        summaryFunction = twoClassSummary,
        classProbs = TRUE,
        allowParallel = TRUE
      )
    )
    
    # Desempenho do modelo
    desempenho <- DesempenhoMedioModelos_KFolds(
      resultados = modelo$pred 
    )
  } else {
    desempenho <- "Nao calculado"
  }
  
  # Modelo de selecao de variaveis
  modelo <- list(
    Variaveis = variaveis,
    Desempenho = desempenho  
  )
  
  # Retorn da funcao
  return(modelo) 
}
####
## Fim
#