#' Simulacoes modelos 
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
Simulador <- function(dados){
  
  # Recarrega parametros
  parSys <<- CarregarParSys()
  
  # Carrega tempo inicial
  ini <- Sys.time()
  
  # Cria tabela para resultados
  resultadosSimulador <- tibble()

  # Processa balanceadores
  balanceador <- parSys$Balanceador$Funcoes[1]
    
  # Processa Selecao de Variaveis
  seletorVariaveis <- parSys$SeletorVariaveis$Funcoes[1]
      
  # Processa classificadores
  classificador <- parSys$Classificador$Funcoes[1]
        
  # Processa Simulacoes 
  simulacao <- 1

  # Processa balanceadores
  for(balanceador in parSys$Balanceador$Funcoes){
    
    # Processa Selecao de Variaveis
    for(seletorVariaveis in parSys$SeletorVariaveis$Funcoes){
      
      # Processa classificadores
      for(classificador in parSys$Classificador$Funcoes){
        
        # Processa Simulacoes 
        for(simulacao in 1L:parSys$Geral$Simulacoes){
          
          # Exibe processamento
          Msg(
            cyan("Simulacao        : "), yellow(simulacao), 
            cyan("\nBalanceador      : "), yellow(balanceador),
            cyan("\nSeletor Variaveis: "), yellow(seletorVariaveis), 
            cyan("\nClassificador    : "), yellow(classificador)
          )
          
          # Verifica se ja processou
          processou <- resultadosSimulador %>% 
            Processou(
              simulacao = simulacao,
              balanceador = balanceador,
              seletorVariaveis = seletorVariaveis,
              classificador = classificador 
            )
          
          # # Se ja processou
          # if(processou){
          #   next()
          # }
          # 
          # Selecao de variaveis
          varSelecionadas <- ChamarFuncao(
            dados = dados,
            funcao = seletorVariaveis
          )
          
          # Particionamento dos dados em CalibracaoOriginal, CalibracaoBalanceado e Validacao
          dadosSimulacao <- ParticionarDados(
            dados = dados %>% 
              select(Classe, !!varSelecionadas),
            balanceador = balanceador
          )
          
          print(dadosSimulacao$CalibracaoBalanceado %>% head(10))
          
          # Se houve erro 
          if(Erro(dadosSimulacao)){
            next()
          }
          
          # Selecao de variaveis
          #varSelecionadas <- ChamarFuncao(
          #  dados = dadosSimulacao$CalibracaoBalanceado,
          #  funcao = seletorVariaveis
          #)
          
          # Se houve erro 
          if(Erro(varSelecionadas)){
            next()
          }
          
          # Cria modelo preditivo para Validacao
          modelo <- ChamarFuncao(
            dados = dadosSimulacao$CalibracaoBalanceado %>% 
              select(Classe, !!varSelecionadas),
            funcao = classificador
          )
          
          # Se houve erro 
          if(Erro(modelo)){
            next()
          }
          
          # Armazena resultados da Validacao 
          resultadosSimulador %<>% 
            bind_rows(
              ResultadosSimulador(
                dadosTeste = dadosSimulacao$Validacao,
                simulacao = simulacao,
                balanceador = balanceador,
                seletorVariaveis = seletorVariaveis,
                varSelecionadas = varSelecionadas,
                modelo = modelo,
                classificador = classificador
              )
            )
          
          # Salva resultados do simulador
          # SaveResultadosSimulador(resultadosSimulador)
        }
      }
    }
  } 
  
  # Mensagem com o tempo de processamento
  MsgTempoProcessamento(ini)
  
  # Retorno da funcao
  return(resultadosSimulador)
}
####
## Fim
#
