#' Desempenho medio dos modelos
#'
#' @return
#' @export
#'
#' @examples
DesempenhoMedioModelos_KFolds <- function(resultados){
  
  # Desempenho medio dos modelos
  resultados %<>% 
    split(.$Resample) %>% 
    lapply(
      FUN = function(fold){
        
        matrizConfusao <- caret::confusionMatrix(
          data = fold$pred ,
          reference = fold$obs,
          mode = "everything"
        )
        
        matrizConfusao$AUC_ROC <- pROC::roc(
          response = fold$obs %>% 
            factor(
              levels = c("Sim", "Nao"),
              ordered = TRUE
            ),
          predictor = fold$pred %>% 
            factor(
              levels = c("Sim", "Nao"),
              ordered = TRUE
            ),
          quiet = TRUE
        )$auc %>% 
          as.numeric()
        
        tibble(
          Fold = fold$Resample[1],
          Acuracia = matrizConfusao$overall[1],
          Kappa = matrizConfusao$overall[2],
          Sensibilidade = matrizConfusao$byClass[1],
          Especificidade = matrizConfusao$byClass[2],
          AUC_ROC = matrizConfusao$AUC_ROC,
          F1 = matrizConfusao$byClass[7]
        )
      }
    ) %>% 
    bind_rows() 
  
  # Ajusta resultados
  resultados %<>% 
    summarise(
      Medida = "Acuracia",
      Media = mean(Acuracia, na.rm = TRUE),
      IC_Inicial = gmodels::ci(
        x = Acuracia,
        confidence = 0.95
      )[2],
      IC_Final = gmodels::ci(
        x = Acuracia,
        confidence = 0.95
      )[3]
    ) %>% 
    bind_rows(
      resultados %>% 
        summarise(
          Medida = "Kappa",
          Media = mean(Kappa, na.rm = TRUE),
          IC_Inicial = gmodels::ci(
            x = Kappa,
            confidence = 0.95
          )[2],
          IC_Final = gmodels::ci(
            x = Kappa,
            confidence = 0.95
          )[3]
        ) 
    ) %>% 
    bind_rows(
      resultados %>% 
        summarise(
          Medida = "Sensibilidade",
          Media = mean(Sensibilidade, na.rm = TRUE),
          IC_Inicial = gmodels::ci(
            x = Sensibilidade,
            confidence = 0.95
          )[2],
          IC_Final = gmodels::ci(
            x = Sensibilidade,
            confidence = 0.95
          )[3]
        ) 
    ) %>% 
    bind_rows(
      resultados %>% 
        summarise(
          Medida = "Especificidade",
          Media = mean(Especificidade, na.rm = TRUE),
          IC_Inicial = gmodels::ci(
            x = Especificidade,
            confidence = 0.95
          )[2],
          IC_Final = gmodels::ci(
            x = Especificidade,
            confidence = 0.95
          )[3]
        ) 
    ) %>% 
    bind_rows(
      resultados %>% 
        summarise(
          Medida = "AUC_ROC",
          Media = mean(AUC_ROC, na.rm = TRUE),
          IC_Inicial = gmodels::ci(
            x = AUC_ROC,
            confidence = 0.95
          )[2],
          IC_Final = gmodels::ci(
            x = AUC_ROC,
            confidence = 0.95
          )[3]
        ) 
    ) %>% 
    bind_rows(
      resultados %>% 
        summarise(
          Medida = "F1",
          Media = mean(F1, na.rm = TRUE),
          IC_Inicial = gmodels::ci(
            x = F1,
            confidence = 0.95
          )[2],
          IC_Final = gmodels::ci(
            x = F1,
            confidence = 0.95
          )[3]
        ) 
    ) %>% 
    mutate(
      IC_Final = ifelse(
        test = IC_Final > 1,
        yes = 1,
        no = IC_Final
      )
    )
  
  # Retorno da funcao
  return(resultados)
}
####
## Fim
#