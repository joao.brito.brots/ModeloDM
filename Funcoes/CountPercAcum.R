#' Count com percentual e acumulado
#'
#' @return
#' @export
#'
#' @examples
CountPercAcum <- function(dados, ...){
  
  dados %<>% 
    dplyr::count(..., sort = TRUE, name = "Qtd") %>% 
    mutate(
      Perc = Qtd / sum(Qtd) * 100,
      PercAcum = cumsum(Perc)
    )
  
  # Retorno da funcao
  return(dados)
}
####
## Fim
#