#' Selecao de Variaveis com GMDH
#'
#' @param dadosTreino 
#'
#' @return
#' @export
#'
#' @examples
SelecaoVariaveis_GMDH <- function(dados){
 
  # Adiciona Id
  dados %<>% 
    rowid_to_column(
      var = "Id"
    )
  
  # Dados Treino
  dadosTreino <- dados %>% 
    sample_frac(
      size = 2/3
    )
  
  # Dados Teste
  dadosTeste <- dados %>% 
    anti_join(
      dadosTreino,
      by = "Id"
    )
  
  # Elimina ID
  dadosTreino$Id <- NULL
  dadosTeste$Id <- NULL
  
  # Modelo GMDH
  modelo <- GMDH(
    x.train = dadosTreino %>% 
      select(-Classe) %>% 
      as.matrix(),
    y.train = dadosTreino$Classe,
    x.valid = dadosTeste %>% 
      select(-Classe) %>% 
      as.matrix(),
    y.valid = dadosTeste$Classe
  )
  
  # Retorno da funcao
  return(modelo$features) 
}
####
## Fim
#