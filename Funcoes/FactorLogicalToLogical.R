#' Converte campo fator logical para logical
#'
#' @param vetor 
#'
#' @return
#' @export
#'
#' @examples
FactorLogicalToLogical <- function(vetor){
  
  # Se for campo logical 
  if(EspecialClass(vetor, "FactorLogical")){
    
    # Converte vetor para logico
    vetor <- if_else(
      condition = (vetor == 1),
      true = TRUE,
      false = ifelse(
        test = (vetor == 0),
        yes = FALSE,
        no = NA
      ),  
      missing = NA
    )
  }
  
  # Retorno da funcao  
  return(vetor)
}
####
## Fim
#